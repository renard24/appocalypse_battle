import pygame
import math
import random

class Ennemy(pygame.sprite.Sprite):
    def __init__(self, classe):
        pygame.sprite.Sprite.__init__(self)
        self.x=1268*random.random()
        self.image=pygame.Surface((103, 103))
        im_sprite=random.randint(1,3)
        if classe==1:
            self.image=(pygame.image.load("images/ange"+str(im_sprite)+".png").convert_alpha()).copy()
            self.y=220
            self.image=pygame.transform.rotate(self.image,180)
            self.dir=pygame.math.Vector2((0,1))
        elif classe==2:
            self.image=(pygame.image.load("images/demon"+str(im_sprite)+".png").convert_alpha()).copy()
            self.y=400
            self.dir=pygame.math.Vector2((0,-1))
        self.image.convert_alpha()
        self.rect=self.image.get_rect(x=self.x,y=self.y)
        self.classe=classe
        self.dx=-0.5
        self.vie=2
        self.t=0
        self.est_mort=False
        self.isblinking=False
        self.k=0
        self.k2=0

    def update(self):
        if self.est_mort:
            self.t+=1
            if self.t==30:
                self.kill()
        else:
            if self.t%200==0:
                self.dx=-self.dx
            self.t+=1
            self.x+=self.dx
            self.rect.x=self.x

    def draw(self,fenetre):
        if not(self.isblinking):
            fenetre.blit(self.image,self.rect)
        else:
            self.k+=1
            if self.k>=5 and self.k<10:
                fenetre.blit(self.image,self.rect)
            if self.k>10:
                self.k=0
                self.k2+=1
            if self.k2==4:
                self.k2=0
                self.isblinking=0

    def mort(self):
        self.dx=0
        self.image=(pygame.image.load("images/splash"+str(self.classe)+".png").convert_alpha()).copy()
        self.image=pygame.transform.scale(self.image,(83,83))
        self.est_mort=True
        self.t=0
