import pygame
import random
import math

class Objet(pygame.sprite.Sprite):
    def __init__(self, x, y, classe):
        pygame.sprite.Sprite.__init__(self)
        self.x=x
        self.y=y
        self.angle=360*random.random()-180
        self.dir=pygame.math.Vector2()
        self.dir.from_polar((2,self.angle))
        self.image=pygame.Surface((32, 32))
        self.image=(pygame.image.load("images/upgrade"+str(classe)+".png").convert_alpha()).copy()
        self.image=pygame.transform.scale(self.image,(32,32))
        self.rect=self.image.get_rect(x=self.x,y=self.y)
        self.dx=self.dir.x
        self.dy=self.dir.y
        self.dist=0
        self.classe=classe
        self.t=0

    def update(self):
        self.t+=1
        if self.t%10==0:
            self.angle=360*random.random()-180
            self.dir.from_polar((2,self.angle))
        self.x+=self.dx
        self.rect.x+=self.dx
        self.y+=self.dy
        self.rect.y+=self.dy
        self.dist+=math.sqrt((self.dx)**2 + (self.dy)**2)

    def draw(self,fenetre):
        fenetre.blit(self.image,self.rect)
