import pygame
from pygame.locals import *

def game_over(fenetre, winner):
    font_score=pygame.font.Font(None,70)
    if winner==1:
        decor=(pygame.image.load("images/font1.png").convert_alpha()).copy()
        decor=pygame.transform.rotozoom(decor,0,0.75)
        text=font_score.render("LE JOUEUR 1 GAGNE !",False,(0,0,0))
    elif winner==2:
        decor=(pygame.image.load("images/font2.png").convert_alpha()).copy()
        decor=pygame.transform.rotozoom(decor,0,0.72)
        text=font_score.render("LE JOUEUR 2 GAGNE !",False,(0,0,0))
    replay=False

    while not replay:
        fenetre.blit(decor,(0,0))
        fenetre.blit(text,(425,350))
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                replay = True
