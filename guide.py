import pygame
from pygame.locals import *
from scene_multi import *

def create_guide(fenetre):
    fond=(pygame.image.load("images/touches.png").convert_alpha()).copy()
    fond=pygame.transform.scale(fond,fenetre.get_size())
    guide=True

    while guide:
        fenetre.blit(fond,(0,0))
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                guide = False
            elif event.type == KEYDOWN and event.key == K_RETURN:
                create_scene_multi(fenetre)
        pygame.display.flip()
