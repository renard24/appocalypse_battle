import pygame
from pygame.locals import *

def game_over(fenetre, score, color_font):
    decor=(pygame.image.load("images/game_over.png").convert_alpha()).copy()
    decor=pygame.transform.rotozoom(decor,0,0.75)
    font_score=pygame.font.Font(None,60)
    replay=False

    while not replay:
        fenetre.blit(decor,(0,-75))
        fenetre.blit(font_score.render("Score : "+str(score),False,color_font),(580,300))
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                replay = True
