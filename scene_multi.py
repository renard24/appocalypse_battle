import pygame
from pygame.locals import *
import random
from perso2 import *
from ennemy2 import *
from objets import *
from game_over_multi import *

def create_scene_multi(fenetre):
    perso1=Perso(1,5)
    perso2=Perso(2,5)
    groupe_anges=pygame.sprite.RenderUpdates()
    groupe_demons=pygame.sprite.RenderUpdates()
    groupe_tirs_anges=pygame.sprite.RenderUpdates()
    groupe_tirs_demons=pygame.sprite.RenderUpdates()
    groupe_objets=pygame.sprite.RenderUpdates()
    groupe_morts=pygame.sprite.RenderUpdates()
    game=True
    spe_tir_j1=10
    spe_tir_j2=10
    pow_tir_j1=1
    pow_tir_j2=1
    cadence_j1=30
    cadence_j2=30
    t1=0
    t2=0
    pygame.key.set_repeat(30,30)
    decor=(pygame.image.load("images/font3.png").convert_alpha()).copy()
    decor=pygame.transform.scale(decor,fenetre.get_size())

    while game:
        fenetre.blit(decor,(0,0))

        perso1.dx=0
        perso1.dy=0
        perso2.dx=0
        perso2.dy=0

        proba=random.random()
        if proba<0.0075:
            ange=Ennemy(1)
            groupe_anges.add(ange)

        proba=random.random()
        if proba<0.0075:
            demon=Ennemy(2)
            groupe_demons.add(demon)

        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                game = False
        liste_key = pygame.key.get_pressed()
        if liste_key[K_LEFT]:
            perso2.dx-=4
        if liste_key[K_RIGHT]:
            perso2.dx+=4
        if liste_key[K_UP]:
            perso2.dy-=4
        if liste_key[K_DOWN]:
            perso2.dy+=4
        if liste_key[K_q]:
            perso1.dx-=4
        if liste_key[K_d]:
            perso1.dx+=4
        if liste_key[K_z]:
            perso1.dy-=4
        if liste_key[K_s]:
            perso1.dy+=4
        if liste_key[K_SPACE]:
            if t1%cadence_j1==0:
                if perso1.nb_tir==1:
                    tir=Projectile(perso1.x+40,perso1.y+40,perso1.classe,perso1.dir,spe_tir_j1,pow_tir_j1)
                    groupe_tirs_anges.add(tir)
                elif perso1.nb_tir==2:
                    tir=Projectile(perso1.x+40,perso1.y+40,perso1.classe,perso1.dir.rotate(-20),spe_tir_j1,pow_tir_j1)
                    groupe_tirs_anges.add(tir)
                    tir=Projectile(perso1.x+40,perso1.y+40,perso1.classe,perso1.dir.rotate(20),spe_tir_j1,pow_tir_j1)
                    groupe_tirs_anges.add(tir)
                elif perso1.nb_tir==3:
                    tir=Projectile(perso1.x+40,perso1.y+40,perso1.classe,perso1.dir.rotate(-30),spe_tir_j1,pow_tir_j1)
                    groupe_tirs_anges.add(tir)
                    tir=Projectile(perso1.x+40,perso1.y+40,perso1.classe,perso1.dir,spe_tir_j1,pow_tir_j1)
                    groupe_tirs_anges.add(tir)
                    tir=Projectile(perso1.x+40,perso1.y+40,perso1.classe,perso1.dir.rotate(30),spe_tir_j1,pow_tir_j1)
                    groupe_tirs_anges.add(tir)
            t1+=1
        elif not liste_key[K_SPACE]:
            t1=0
        if liste_key[K_KP0]:
            if t2%cadence_j2==0:
                if perso2.nb_tir==1:
                    tir=Projectile(perso2.x+40,perso2.y+40,perso2.classe,perso2.dir,spe_tir_j2,pow_tir_j1)
                    groupe_tirs_demons.add(tir)
                elif perso2.nb_tir==2:
                    tir=Projectile(perso2.x+40,perso2.y+40,perso2.classe,perso2.dir.rotate(-20),spe_tir_j2,pow_tir_j2)
                    groupe_tirs_demons.add(tir)
                    tir=Projectile(perso2.x+40,perso2.y+40,perso2.classe,perso2.dir.rotate(20),spe_tir_j2,pow_tir_j2)
                    groupe_tirs_demons.add(tir)
                elif perso2.nb_tir==3:
                    tir=Projectile(perso2.x+40,perso2.y+40,perso2.classe,perso2.dir.rotate(-30),spe_tir_j2,pow_tir_j2)
                    groupe_tirs_demons.add(tir)
                    tir=Projectile(perso2.x+40,perso2.y+40,perso2.classe,perso2.dir,spe_tir_j2,pow_tir_j2)
                    groupe_tirs_demons.add(tir)
                    tir=Projectile(perso2.x+40,perso2.y+40,perso2.classe,perso2.dir.rotate(30),spe_tir_j2,pow_tir_j2)
                    groupe_tirs_demons.add(tir)
            t2+=1
        elif not liste_key[K_KP0]:
            t2=0
        perso1.update()
        perso2.update()

        for ange in groupe_anges:
            ange.update()
            ange.draw(fenetre)
            proba=random.random()
            if proba<0.0075:
                tir=Projectile(ange.x+40,ange.y+40,ange.classe,ange.dir,10,1)
                groupe_tirs_anges.add(tir)

        for demon in groupe_demons:
            demon.update()
            demon.draw(fenetre)
            proba=random.random()
            if proba<0.0075:
                tir=Projectile(demon.x+40,demon.y+40,demon.classe,demon.dir,10,1)
                groupe_tirs_demons.add(tir)

        for tir in groupe_tirs_demons:
            tir.update()
            tir.draw(fenetre)
            if tir.x>1450 or tir.x<-10 or tir.y>790 or tir.y<-10:
                tir.kill()

        for tir in groupe_tirs_anges:
            tir.update()
            tir.draw(fenetre)
            if tir.x>1450 or tir.x<-10 or tir.y>790 or tir.y<-10:
                tir.kill()

        for mort in groupe_morts:
            mort.update()
            mort.draw(fenetre)

        proba=random.random()
        if proba<0.0005:
            x_obj=700*random.random()+300
            y_obj=350*random.random()+150
            classe_obj=random.randint(1,3)
            objet=Objet(x_obj,y_obj,classe_obj)
            groupe_objets.add(objet)

        for objet in groupe_objets:
            objet.update()
            objet.draw(fenetre)
            if objet.dist>30000:
                objet.kill()

        dic_collide1=pygame.sprite.groupcollide(groupe_anges,groupe_tirs_demons,False,True)
        for ange in dic_collide1:
            ange.vie-=1
            ange.isblinking=True
            if ange.vie==0:
                ange.isblinking=False
                ange.mort()
                groupe_anges.remove(ange)
                groupe_morts.add(ange)

        dic_collide2=pygame.sprite.groupcollide(groupe_demons,groupe_tirs_anges,False,True)
        for demon in dic_collide2:
            demon.vie-=1
            demon.isblinking=True
            if demon.vie==0:
                demon.isblinking=False
                demon.mort()
                groupe_demons.remove(demon)
                groupe_morts.add(demon)

        dic_collide3=pygame.sprite.groupcollide(groupe_objets,groupe_tirs_anges,True,True)
        for objet in dic_collide3:
            if objet.classe==1 and perso1.nb_tir<3:
                perso1.nb_tir+=1
            elif objet.classe==2 and spe_tir_j1<30:
                cadence_j1-=5
                spe_tir_j1+=5
            elif objet.classe==3 and pow_tir_j1<3:
                pow_tir_j1+=1

        dic_collide4=pygame.sprite.groupcollide(groupe_objets,groupe_tirs_demons,True,True)
        for objet in dic_collide4:
            if objet.classe==1 and perso2.nb_tir<3:
                perso2.nb_tir+=1
            elif objet.classe==2 and spe_tir_j2<30:
                cadence_j2-=5
                spe_tir_j2+=5
            elif objet.classe==3 and pow_tir_j2<3:
                pow_tir_j2+=1

        liste_degat_anges=pygame.sprite.spritecollide(perso2,groupe_tirs_anges,False)
        if liste_degat_anges!=[] and not perso2.isblinking==1:
            for tir in liste_degat_anges:
                tir.kill()
            perso2.degat()
            if perso2.vie==0:
                game=False
                game_over(fenetre,1)

        liste_degat_demons=pygame.sprite.spritecollide(perso1,groupe_tirs_demons,False)
        if liste_degat_demons!=[] and not perso1.isblinking==1:
            for tir in liste_degat_demons:
                tir.kill()
            perso1.degat()
            if perso1.vie==0:
                game=False
                game_over(fenetre,2)


        perso1.draw(fenetre)
        perso2.draw(fenetre)
        pygame.display.flip()
