import pygame
from pygame.locals import *
from scene_solo import *

def create_choix(fenetre):
    fond=(pygame.image.load("images/ecran_titre.png").convert_alpha()).copy()
    camp_ange=(pygame.image.load("images/bouton_ange.png").convert_alpha()).copy()
    camp_ange_select=(pygame.image.load("images/bouton_ange_surligne.png").convert_alpha()).copy()
    camp_demon=(pygame.image.load("images/bouton_demon.png").convert_alpha()).copy()
    camp_demon_select=(pygame.image.load("images/bouton_demon_surligne.png").convert_alpha()).copy()
    camp_ange_h=camp_ange.get_height()
    camp_ange_w=camp_ange.get_width()
    camp_demon_h=camp_demon.get_height()
    camp_demon_w=camp_demon.get_width()

    selection=True

    while selection:
        fenetre.blit(fond,(0,0))
        mouse_pos=pygame.mouse.get_pos()
        mouse_x=mouse_pos[0]
        mouse_y=mouse_pos[1]
        mouse_pressed=pygame.mouse.get_pressed()
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                selection = False
        if mouse_x<=50+camp_ange_w and mouse_x>=50 and mouse_y<=100+camp_ange_h and mouse_y>=100:
            fenetre.blit(camp_ange_select,(50,100))
            if mouse_pressed[0]:
                create_scene_solo(fenetre,1)
        else:
            fenetre.blit(camp_ange,(50,100))
        if mouse_x<=675+camp_demon_w and mouse_x>=675 and mouse_y<=100+camp_demon_h and mouse_y>=100:
            fenetre.blit(camp_demon_select,(675,100))
            if mouse_pressed[0]:
                create_scene_solo(fenetre,2)
        else:
            fenetre.blit(camp_demon,(675,100))
        pygame.display.flip()
