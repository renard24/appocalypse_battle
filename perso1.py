import pygame

class Perso(pygame.sprite.Sprite):
    def __init__(self, classe, vie):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface((103, 103))
        self.coeur=pygame.Surface((32, 32))
        if classe==1:
            self.image=(pygame.image.load("images/ange1.png").convert_alpha()).copy()
            self.coeur=(pygame.image.load("images/coeur2.png").convert_alpha()).copy()
        elif classe==2:
            self.image=(pygame.image.load("images/demon1.png").convert_alpha()).copy()
            self.coeur=(pygame.image.load("images/coeur1.png").convert_alpha()).copy()
        self.coeur=pygame.transform.scale(self.coeur,(32,32))
        self.image.convert_alpha()
        self.coeur.convert_alpha()
        self.rect=self.image.get_rect(center=(668,384))
        self.image_rot=self.image.copy()
        #self.rect.x=620
        #self.rect.y=320
        self.x=self.rect.x
        self.y=self.rect.y
        self.classe=classe
        self.angle=0
        self.dangle=0
        self.vie=vie
        self.nb_tir=1
        self.dir=pygame.math.Vector2((0,-1))
        #self.vecteur_tir.from_polar((1,0))
        self.isblinking=False
        self.k=0
        self.k2=0

    def update(self):
        self.angle-=self.dangle
        self.image_rot=pygame.transform.rotate(self.image,self.angle)
        self.dir.rotate_ip(self.dangle)
        self.rect=self.image.get_rect(center=(668,384))

    def draw(self,fenetre):
        if not(self.isblinking):
            fenetre.blit(self.image_rot,self.rect)
        else:
            self.k+=1
            if self.k>=5 and self.k<10:
                fenetre.blit(self.image_rot,self.rect)
            if self.k>10:
                self.k=0
                self.k2+=1
            if self.k2==4:
                self.k2=0
                self.isblinking=0
        self.barre_de_vie(fenetre)

    def degat(self):
        self.isblinking=True
        self.vie-=1

    def barre_de_vie(self,fenetre):
        for i in range(self.vie):
            fenetre.blit(self.coeur,(50+i*35,600))


class Projectile(pygame.sprite.Sprite):
    def __init__(self, classe, direction, vitesse, puissance):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([31, 31])
        if classe==1:
            self.image=(pygame.image.load("images/projectile_ange.png").convert_alpha()).copy()
        elif classe==2:
            self.image=(pygame.image.load("images/projectile_demon.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.image=pygame.transform.rotozoom(self.image,0,1+puissance/2)
        self.rect=self.image.get_rect(center=(668,384))
        self.x=668
        self.y=384
        self.pow=puissance
        self.dir=direction
        self.dx=direction.x*vitesse
        self.dy=direction.y*vitesse

    def update(self):
        self.x+=self.dx
        self.rect.x+=self.dx
        self.y+=self.dy
        self.rect.y+=self.dy

    def draw(self,fenetre):
        fenetre.blit(self.image,self.rect)
