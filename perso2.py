import pygame

class Perso(pygame.sprite.Sprite):
    def __init__(self, classe, vie):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface((103, 103))
        self.coeur=pygame.Surface((32, 32))
        if classe==1:
            self.image=(pygame.image.load("images/ange1.png").convert_alpha()).copy()
            self.rect=self.image.get_rect(center=(200,100))
            self.dir=pygame.math.Vector2((0,1))
            self.coeur=(pygame.image.load("images/coeur2.png").convert_alpha()).copy()
        elif classe==2:
            self.image=(pygame.image.load("images/demon1.png").convert_alpha()).copy()
            self.rect=self.image.get_rect(center=(900,600))
            self.image=pygame.transform.rotate(self.image,180)
            self.dir=pygame.math.Vector2((0,-1))
            self.coeur=(pygame.image.load("images/coeur1.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.coeur=pygame.transform.scale(self.coeur,(32,32))
        self.coeur.convert_alpha()
        self.image=pygame.transform.rotate(self.image,180)
        self.x=self.rect.x
        self.y=self.rect.y
        self.classe=classe
        self.vie=vie
        self.nb_tir=1
        self.dx=0
        self.dy=0
        self.isblinking=False
        self.k=0
        self.k2=0

    def update(self):
        self.x+=self.dx
        self.y+=self.dy
        if self.x<0:
            self.x=0
        if self.x>1268:
            self.x=1268
        if self.y<0:
            self.y=0
        if self.y>616:
            self.y=616
        self.rect.x=self.x
        self.rect.y=self.y

    def draw(self,fenetre):
        if not(self.isblinking):
            fenetre.blit(self.image,self.rect)
        else:
            self.k+=1
            if self.k>=5 and self.k<10:
                fenetre.blit(self.image,self.rect)
            if self.k>10:
                self.k=0
                self.k2+=1
            if self.k2==4:
                self.k2=0
                self.isblinking=0
        self.barre_de_vie(fenetre)

    def degat(self):
        self.isblinking=True
        self.vie-=1

    def barre_de_vie(self,fenetre):
        for i in range(self.vie):
            if self.classe==1:
                fenetre.blit(self.coeur,(50+i*35,50))
            elif self.classe==2:
                fenetre.blit(self.coeur,(1100+i*35,650))


class Projectile(pygame.sprite.Sprite):
    def __init__(self, x, y, classe, direction, vitesse, puissance):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([31, 31])
        if classe==1:
            self.image=(pygame.image.load("images/projectile_ange.png").convert_alpha()).copy()
        elif classe==2:
            self.image=(pygame.image.load("images/projectile_demon.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.image=pygame.transform.rotozoom(self.image,0,1+puissance/2)
        self.rect=self.image.get_rect(center=(x+40,y+40))
        self.x=x
        self.y=y
        self.pow=puissance
        self.dir=direction
        self.dx=direction.x*vitesse
        self.dy=direction.y*vitesse

    def update(self):
        self.x+=self.dx
        self.rect.x=self.x
        self.y+=self.dy
        self.rect.y=self.y

    def draw(self,fenetre):
        fenetre.blit(self.image,self.rect)
