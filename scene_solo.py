import pygame
from pygame.locals import *
from perso1 import *
from ennemy1 import *
from objets import *
from game_over import *
import random

def create_scene_solo(fenetre,classe):
    perso=Perso(classe,5)
    if classe==2:
        classe_ennemy=1
        decor=(pygame.image.load("images/font2.png").convert_alpha()).copy()
        decor=pygame.transform.scale(decor,fenetre.get_size())
        color_font=(50,50,200)
    elif classe==1:
        classe_ennemy=2
        decor=(pygame.image.load("images/font1.png").convert_alpha()).copy()
        decor=pygame.transform.scale(decor,fenetre.get_size())
        color_font=(200,25,25)
    groupe_ennemies=pygame.sprite.RenderUpdates()
    groupe_tirs=pygame.sprite.RenderUpdates()
    groupe_objets=pygame.sprite.RenderUpdates()
    groupe_ennemies_morts=pygame.sprite.RenderUpdates()
    game=True
    p=0
    t=0
    font_score=pygame.font.Font(None,30)
    score=0
    spe_tir=10
    pow_tir=1
    cadence=30

    while game:
        p+=0.0007
        fenetre.blit(decor,(0,0))
        fenetre.blit(font_score.render("Score : "+str(score),False,color_font),(100,100))
        perso.dangle=0

        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                game = False

        mouse_pressed=pygame.mouse.get_pressed()
        if mouse_pressed[0]:
            if t%cadence==0:
                if perso.nb_tir==1:
                    tir=Projectile(perso.classe,perso.dir,spe_tir,pow_tir)
                    groupe_tirs.add(tir)
                elif perso.nb_tir==2:
                    tir=Projectile(perso.classe,perso.dir.rotate(-20),spe_tir,pow_tir)
                    groupe_tirs.add(tir)
                    tir=Projectile(perso.classe,perso.dir.rotate(20),spe_tir,pow_tir)
                    groupe_tirs.add(tir)
                elif perso.nb_tir==3:
                    tir=Projectile(perso.classe,perso.dir.rotate(-30),spe_tir,pow_tir)
                    groupe_tirs.add(tir)
                    tir=Projectile(perso.classe,perso.dir,spe_tir,pow_tir)
                    groupe_tirs.add(tir)
                    tir=Projectile(perso.classe,perso.dir.rotate(30),spe_tir,pow_tir)
                    groupe_tirs.add(tir)
            t+=1
        elif not mouse_pressed[1]:
            t=0

        for tir in groupe_tirs:
            tir.update()
            tir.draw(fenetre)
            if tir.x>1450 or tir.x<-10 or tir.y>790 or tir.y<-10:
                tir.kill()

        proba=random.random()
        if proba<0.01+0.005*int(p):
            x_en=668
            y_en=384
            while x_en>=-5 and x_en<=1390:
                x_en=3*1336*random.random()-1336
            while y_en>=-5 and y_en<=780:
                y_en=3*768*random.random()-768
            ennemy=Ennemy(x_en,y_en,classe_ennemy,1)
            groupe_ennemies.add(ennemy)

        proba=random.random()
        if proba<0.0005:
            x_obj=1336*random.random()
            y_obj=768*random.random()
            classe_obj=random.randint(1,3)
            objet=Objet(x_obj,y_obj,classe_obj)
            groupe_objets.add(objet)

        for ennemy in groupe_ennemies:
            ennemy.update()
            ennemy.draw(fenetre)
            if ennemy.dist>60000:
                ennemy.kill()

        for objet in groupe_objets:
            objet.update()
            objet.draw(fenetre)
            if objet.dist>30000:
                objet.kill()

        for ennemy in groupe_ennemies_morts:
            ennemy.update()
            ennemy.draw(fenetre)

        perso.draw(fenetre)
        pygame.display.flip()

        mouse_pos=pygame.mouse.get_pos()
        mouse_dir=pygame.math.Vector2(mouse_pos[0]-668,mouse_pos[1]-384)
        perso.dangle=perso.dir.angle_to(mouse_dir)
        perso.update()

        dic_collide1=pygame.sprite.groupcollide(groupe_ennemies,groupe_tirs,False,True)
        for ennemy in dic_collide1:
            ennemy.vie-=1
            if ennemy.vie==0:
                ennemy.mort()
                groupe_ennemies.remove(ennemy)
                groupe_ennemies_morts.add(ennemy)
                score+=1

        dic_collide2=pygame.sprite.groupcollide(groupe_objets,groupe_tirs,True,True)
        for objet in dic_collide2:
            if objet.classe==1 and perso.nb_tir<3:
                perso.nb_tir+=1
            elif objet.classe==2 and spe_tir<30:
                cadence-=5
                spe_tir+=5
            elif objet.classe==3 and pow_tir<3:
                pow_tir+=1

        liste_collide_ennemy=pygame.sprite.spritecollide(perso,groupe_ennemies,False)
        if liste_collide_ennemy!=[] and not perso.isblinking==1:
            for ennemy in liste_collide_ennemy:
                ennemy.kill()
            perso.degat()
            if perso.vie==0:
                game=False
                game_over(fenetre,score, color_font)
