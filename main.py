import pygame
from pygame.locals import *
from selection import *
from guide import *

pygame.init()

fenetre=pygame.display.set_mode()
fond=(pygame.image.load("images/ecran_titre.png").convert_alpha()).copy()

game=True
font_menu=pygame.font.Font(None,40)
font_menu_select=pygame.font.Font(None,50)
font_titre=pygame.font.Font("Rockit.ttf",60)
i=0
pygame.mouse.set_visible(True)
text1=font_menu.render("1 Joueur", False, (150,150,0))
text1_select=font_menu_select.render("1 Joueur", False, (200,200,0))
text2=font_menu.render("2 Joueur", False, (150,150,0))
text2_select=font_menu_select.render("2 Joueur", False, (200,200,0))
text1_h=text1.get_height()
text1_w=text1.get_width()
text2_h=text2.get_height()
text2_w=text2.get_width()
text_titre=font_titre.render("Apocalypse Battle", False, (0,0,0))
pygame.display.set_caption("Apocalypse Battle")

while game:
    fenetre.blit(fond,(0,0))
    mouse_pos=pygame.mouse.get_pos()
    mouse_x=mouse_pos[0]
    mouse_y=mouse_pos[1]
    mouse_pressed=pygame.mouse.get_pressed()
    fenetre.blit(text_titre,(325,100))
    for event in pygame.event.get():
        if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
            game = False
    if mouse_x<=620+text1_w and mouse_x>=620 and mouse_y<=450+text1_h and mouse_y>=450:
        fenetre.blit(text1_select,(610,445))
        if mouse_pressed[0]:
            create_choix(fenetre)
    else:
        fenetre.blit(text1,(620,450))
    if mouse_x<=620+text2_w and mouse_x>=620 and mouse_y<=500+text2_h and mouse_y>=500:
        fenetre.blit(text2_select,(610,495))
        if mouse_pressed[0]:
            create_guide(fenetre)
    else:
        fenetre.blit(text2,(620,500))
    pygame.display.flip()
pygame.quit()
