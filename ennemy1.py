import pygame
import math
import random

class Ennemy(pygame.sprite.Sprite):
    def __init__(self, x, y, classe, vie):
        pygame.sprite.Sprite.__init__(self)
        self.x_depart=x
        self.y_depart=y
        self.x=self.x_depart
        self.y=self.y_depart
        self.direction=pygame.math.Vector2((668-x,384-y)).normalize()
        self.image=pygame.Surface((103, 103))
        im_sprite=random.randint(1,3)
        if classe==1:
            self.image=(pygame.image.load("images/ange"+str(im_sprite)+".png").convert_alpha()).copy()
        elif classe==2:
            self.image=(pygame.image.load("images/demon"+str(im_sprite)+".png").convert_alpha()).copy()
        rotation=(self.direction.as_polar()[1])
        self.image=pygame.transform.rotate(self.image,270-rotation)
        self.image.convert_alpha()
        self.rect=self.image.get_rect(x=self.x,y=self.y)
        self.classe=classe
        self.dx=self.direction.x*3
        self.dy=self.direction.y*3
        self.dist=0
        self.vie=vie
        self.t=0
        self.est_mort=False

    def update(self):
        if self.est_mort:
            self.t+=1
            if self.t==30:
                self.kill()
        else:
            self.x+=self.dx
            self.rect.x+=self.dx
            self.y+=self.dy
            self.rect.y+=self.dy
            self.dist=math.sqrt((self.x_depart-self.x)**2 + (self.y_depart-self.y)**2)

    def draw(self,fenetre):
        fenetre.blit(self.image,self.rect)

    def mort(self):
        self.dx=0
        self.dy=0
        self.image=(pygame.image.load("images/splash"+str(self.classe)+".png").convert_alpha()).copy()
        self.est_mort=True
